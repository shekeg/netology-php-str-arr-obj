<?php
  $continents = [
    'Africa' => [
      'Lemur',
      'Mandrill'
    ],
    'Asia' => [
      'Bengal Tiger',
      'Japanese Macaque',
      'Giant Panda'
    ],
    'Europe'=> [
      'Raccoon Dog',
      'Pine Marten'
    ],
    'America' => [
      'American Black Bear',
      'Mountain Lions'
    ],
    'Antarctica' => [
      'Emperor Penguins',
      'Snow Petrel',
      'Weddell Seals'
    ],
    'Australia' => [
      'Red Kangaroo',
      'Koala',
      'Australian Dingo'
    ]
  ];
  
  function showFantasticAnimals($continents) {
    $words = [];
    $wordsFirst = [];
    $wordsSecond = [];
    
    foreach ($continents as $continent => $animals) {
      foreach ($animals as $animal) { 
        if (count(explode(' ', $animal)) == 2) {
          $animalWithTwoWords = $animal;
          $words = explode(' ', $animalWithTwoWords);
          $wordsFirst[] = $words[0];
          $wordsSecond[] = $words[1];
        }
      }
    }
      
    shuffle($wordsFirst);
    shuffle($wordsSecond);

    $fantasticAnimals = [];
    for ($i = 0; $i < count($wordsFirst); $i++) {
      $fantasticAnimals[] = "{$wordsFirst[$i]} {$wordsSecond[$i]}";
    }
    
    /* Создание массива с фнтастическими животными сгруппированым по континентам
    в соответствии с доп. заданием №1 (по первому слову) */
    $continentsFantasticAnimals = [];
    $animalFirstWord = [];
    $fantasticAnimalFirstWord = [];
    foreach ($continents as $continent => $animals)  {
      foreach ($animals as $animal) {     
        $animalFirstWord = explode(' ', $animal)[0];
        foreach ($fantasticAnimals as $fantasticAnimal) {
          $fantasticAnimalFirstWord = explode(' ', $fantasticAnimal)[0];
          if ($animalFirstWord == $fantasticAnimalFirstWord) {
            $continentsFantasticAnimals[$continent][] = $fantasticAnimal;
          }
        }
      }
    }

    //Вывод массива с фантастическими животными в предложенном формате
    foreach ($continentsFantasticAnimals as $continentFantasticAnimals => $fantasticAnimals) {
      echo "<h2>{$continentFantasticAnimals}</h2>";
      echo (implode(', ', $fantasticAnimals));
    }
  } 

  showFantasticAnimals($continents);
?>